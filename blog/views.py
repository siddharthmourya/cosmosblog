from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
import pymongo
from datetime import datetime
from bson.objectid import ObjectId

# Create your views here.
def index(request):
    mongoclient = pymongo.MongoClient("mongodb://localhost:27017/")
    db = mongoclient["blog"]
    post = db["post"]
    result_cur = db.post.find({})
    result = list(result_cur)
    for data in result:
        data['id'] = data['_id']
        del data['_id'] 
    result_data = {                  #doing this because context must be dict not listr at present it is list
        "result":result,
    }

    print(result_data)
    return render(request , "index.html" , result_data)

def contact(request):
    return render(request , "contact.html")
def post(request , id):
    mongoclient = pymongo.MongoClient("mongodb://localhost:27017/")
    db = mongoclient["blog"]
    post = db["post"]
    result_cur = db.post.find({'_id' : ObjectId(id)})
    result = list(result_cur)
    result = result[0]
    print(result)
    return render(request , "post.html" , result)

def about(request):
    return render(request , "about.html")

@csrf_protect
def add(request):
    print("in addpost")
    if request.method == 'POST':
        title = request.POST.get("title")
        article = request.POST.get("article")
        author = request.POST.get("author")
        email = request.POST.get("email")
        today = datetime.today()
        print("title : " , title)
        print("article : " , article)
        print("author : " , author)
        print("email : " , email)
        print("date : " , today)
        post_data = {
            "title" : title,
            "article" : article,
            "date":today,
            "author" : author,
            "email" : email,
         }

        mongoclient = pymongo.MongoClient("mongodb://localhost:27017/")
        db = mongoclient["blog"]
        post = db["post"]
        inserted = db.post.insert(post_data)

        return render(request , "index.html")
    return render(request , "add.html")
