from django.contrib import admin
from django.urls import path
from blog import views

urlpatterns = [
    path('' , views.index , name = "index"),
    path('contact' , views.contact , name = "contact"),
    path('about' , views.about , name = "about"),
    path('post/<str:id>' , views.post , name = "post"),
    path('add' , views.add , name = "add"),
]
